
**Auteur : Vincent Amstoutz**

**Quelles sont les deux façons d'initialiser un dépôt ? (1 point)**

On peut utiliser la commande :

```
 git init
```

pour initialiser un répertoire existant en local et l'importer dans Git

ou bien importer un dépôt distant déjà existant avec la commande :

```
 git clone adresse_du_depot
```

On peut à le choix entre spécifier l'adresse soit au format SSH ou soit au format HTTPS.

**[](#%C3%A0-quoi-sert-une-pull-request-merge-request-1-point)À quoi sert une Pull Request / Merge Request ? (1 point)**

Une `pull request` appelée aussi `merge request` est une revue de code. Ainsi, elle permet de demander aux autres développeurs du projet la validation du code effectué pour le projet. Elle permet de vérifier la qualité du code, de s'entraider (en signalant un code qui pourrait être amélioré pour telle ou telle raison) ou tout simplement de valider le code s'il ne pose pas de problème et ensuite de l'intégrer.

### [](#quest-ce-quune-branche-1-point)Qu'est ce qu'une branche ? (1 point)

Une branche est un pointeur vers un commit. La branche par défaut s’appelle master. Il est utile de créer d'autres branches pour segmenter le travail lors d'une phase de développement et ainsi mieux s'organiser sans risquer de détruire le travail des autres collaborateurs. On peut vérifier les branches disponibles du répertoire avec la commande

```
 git branch
```

ou en créer une puis aller dessus avec la commande

```
 git checkout -b "le_nom_de_ma_branche"
```

D'autres options permettent de faire pleins d'autre choses sur les branch comme la fusion de branches appellé merge.

### [](#vous-venez-de-modifier-un-fichier-comment-cr%C3%A9er-un-commit-3-points)Vous venez de modifier un fichier. Comment créer un commit ? (3 points)

Le fichier qui vient d'être modifié se situe en local dans la zone appellée working directory. Pour créer un commit il faut d'abord ajouter le fichier concerné à la staging area avec la commande :

```
 git add . 
```

Il faut ensuite écrire le commit avec la commande :

```
 git commit -m "mon_message"
```

Notre fichier arrive alors dans la zone "repository". Il ne reste plus qu'a faire cette commande pour pousser son commit sur le repository en ligne :

```
 git push
```

### [](#pourquoi-est-il-plus-prudent-dutiliser-originmaster-plut%C3%B4t-que-master-pour-se-mettre-%C3%A0-jour-1-point)Pourquoi est-il plus prudent d'utiliser `origin/master` plutôt que `master` pour se mettre à jour ? (1 point)

Pour se mettre à jour, si on mets `master` on pointe vers le master distant de la remote sur laquelle on se trouve.  En revanche, si on mets `origin/master` on pointe sur la branche master du dépôt local. Il est préférable d'utiliser cette dernière car dans le cas d'une mise à jour on gardera le commit local. 

### [](#a-quoi-servent-les-commandes-git-status-git-log-et-git-reflog-quand-les-utiliser-2-points)A quoi servent les commandes `git status`, `git log` et `git reflog` ? Quand les utiliser ? (2 points)

La commande `git status` permet d'avoir un état sur son répertoire local. La commande nous permet de consulter la liste des fichiers que l'on a modifiés, supprimés ajoutés en local. La commande nous suggère quoi faire selon la zone dans laquelle on se trouve (faire un add, faire un commit ou un push).

La commande `git log`quand à elle permet d'avoir l'historique des différents commits avec la date, l' email de l'auteur, le message et l'emplacement. Elle est souvent utilisée après plusieurs commits, quand on veut revoir ce qu'on a fait. Elle sert aussi à revoir l'historique avant d'entamer une phase de code.

La commande `git reflog` sert à consulter l'historique de tout ce qui a été fait sur une longue durée (commits, changements de branches, rebases, etc). Elle est en général utilisée pour restaurer d'anciens commits (les nôtres ou ceux d’autres personnes). 

### [](#quelles-sont-les-conditions-qui-doivent-%C3%AAtre-r%C3%A9unies-pour-que-des-conflits-surviennent-2-points)Quelles sont les conditions qui doivent être réunies pour que des conflits surviennent ? (2 points)

Pour qu'il y est des conflits il faut que au moins deux personnes modifient les mêmes fichiers, déposent leur travail sur le dépôt distant alors que quelqu'un à déjà modifié le ou les fichier.

### [](#imaginez-que-la-branche-master-du-d%C3%A9p%C3%B4t-distant-contient-de-nouveaux-commits-comment-int%C3%A9grer-ces-commits-dans-votre-branche-3-points)Imaginez que la branche master du dépôt distant contient de nouveaux commits. Comment intégrer ces commits dans votre branche ? (3 points)*

Pour intégrer les nouveaux commits de la branche master à sa branche il faut d'abord aller sur notre branche avec la commande :

```
 git checkout ma_branche
```

Puis il faut récupérer les commits de master et les intégrer à notre branche avec :

```
git pull origin master
```

Note pour cette question :

>  Après avoir essayé sur une dépôt de test en effet ça ne marche pas. Je me suis rendu compte que je récupérait les éléments du master local et non pas le master du dépôt distant. Je travaillais alors entre les "zones locales" au lieu de communiquer avec le dépôt distant.
Après avoir réfléchis à nouveau, et testé sur mon dépôt de test je me suis aperçu qu'il fallait utiliser la commande `git pull origin master` depuis la branche où l'on veut récupérer les commits. Et là on récupère bien les nouveaux commits de la branche master distante.

Merci pour ton précédent retour !
